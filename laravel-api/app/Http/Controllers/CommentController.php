<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store','update','destroy']);
    }

    public function index()
    {
        $comment = Comment::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'List Data Comment',
            'data'    => $comment 
        ], 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id'   => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $comment = Comment::create([
            'content'  => $request->content,
            'post_id'  => $request->post_id
        ]);

        if($comment) {
            return response()->json([
                'success' => true,
                'message' => 'Comment Created',
                'data'    => $comment
            ], 201);
        }
        
        return response()->json([
            'success' => false,
            'message' => 'Comment Failed to Save',
        ], 409);
    }

    public function show($id)
    {
        $comment = Comment::find($id);

        if($comment){
            return response()->json([
                'success' => true,
                'message' => 'Detail Data Comment',
                'data'    => $comment 
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Detail Data Comment ' . $id .' tidak ditemukan',
        ], 404);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id'   => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $comment = Comment::find($id);

        if($comment) {
            $comment->update([
                'content'  => $request->content,
                'post_id'  => $request->post_id
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Comment  ' . $comment->name .'  Updated',
                'data'    => $comment  
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }

    public function destroy($id)
    {
        $comment = Comment::find($id);

        if($comment) {
            
            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comment Deleted',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }

}
