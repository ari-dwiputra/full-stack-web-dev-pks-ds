<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'List Data Roles',
            'data'    => $roles  
        ], 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $role = Role::create([
            'name'  => $request->name,
        ]);

        if($role) {
            return response()->json([
                'success' => true,
                'message' => 'Role Created',
                'data'    => $role
            ], 201);
        }
        
        return response()->json([
            'success' => false,
            'message' => 'Role Failed to Save',
        ], 409);
    }

    public function show($id)
    {
        $role = Role::find($id);

        if($role){
            return response()->json([
                'success' => true,
                'message' => 'Detail Data Roles',
                'data'    => $role 
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Detail Data Role ' . $id .' tidak ditemukan',
        ], 404);

    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $role = Role::find($id);

        if($role) {
            $role->update([
                'name'     => $request->name,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Role  ' . $role->name .'  Updated',
                'data'    => $role  
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Role Not Found',
        ], 404);

    }

    public function destroy($id)
    {
        $role = Role::find($id);

        if($role) {
            $role->delete();

            return response()->json([
                'success' => true,
                'message' => 'Role Deleted',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Role Not Found',
        ], 404);
    }
}
