<?php

namespace App\Listeners;

use App\Events\OtpCodeEvent;
use App\Mail\RegisterUserMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;


class SendEmailToRegisterUser implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtpCodeEvent  $event
     * @return void
     */
    public function handle(OtpCodeEvent $event)
    {
        Mail::to($event->user->email)->send(new RegisterUserMail($event->user));
    }
}
