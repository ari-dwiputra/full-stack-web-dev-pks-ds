<?php

namespace App\Listeners;

use App\Events\OtpCodeEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailToRegenerateOtp
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtpCodeEvent  $event
     * @return void
     */
    public function handle(OtpCodeEvent $event)
    {
        Mail::to($event->user->email)->send(new RegenerateOtpCodeMail($event->user));
    }
}
