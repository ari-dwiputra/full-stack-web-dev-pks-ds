<?php

trait Hewan{
    public $nama;
    public $darah = 50;
    public $jumlahkaki;
    public $keahlian;

    public function atraksi(){
        return "Ini Hewan ". $this->nama. " dengan jumlah kaki ". $this->jumlahkaki. 
        " memiliki keahlian yang dapat ". $this->keahlian;
    }
}

abstract class Fight{
    use Hewan;

    public $attackPower;
    public $defencePower;


    public function serang($hewan){
        echo $this->nama ." sedang menyerang " . $hewan->nama;
        echo "<br>";
        $hewan->diserang($this);
    }

    public function diserang($hewan){
        echo $this->nama ." sedang diserang " . $hewan->nama;

        $this->darah = $this->darah - ($hewan->attackPower / $this->defencePower);
    }

    protected function getInfo(){
        echo "Nama = {$this->nama}";
        echo "<br>";
        echo "Jumlah kaki = {$this->jumlahkaki}";
        echo "<br>";
        echo "Keahlian = {$this->keahlian}";
        echo "<br>";
        echo "Darah = {$this->darah}";
        echo "<br>";
        echo "Attack power = {$this->attackPower}";
        echo "<br>";
        echo "Defence power = {$this->defencePower}";
    }

    abstract public function getInfoHewan();
}

class Harimau extends Fight{
    
    public function __construct($nama){
        $this->nama = $nama;
        $this->jumlahkaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function getInfoHewan(){
        echo "Jenis hewan = Harimau";
        echo "<br>";
        
        $this->getInfo();
    }
}

class Elang extends Fight{
    
    public function __construct($nama){
        $this->nama = $nama;
        $this->jumlahkaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function getInfoHewan(){
        echo "Jenis hewan = Elang";
        echo "<br>";
        
        $this->getInfo();
    }
}

class Spasi{
    public static function Space(){
        echo "<br>";
        echo "==============";
        echo "<br>";
    }
}

$harimau = new Harimau("Harimau");
$harimau->getInfoHewan();
Spasi::Space();
$elang = new Elang("Elang");
$elang->getInfoHewan();
Spasi::Space();
$harimau->serang($elang);
Spasi::Space();
$elang->getInfoHewan();
?>
