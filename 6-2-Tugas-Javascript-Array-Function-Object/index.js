// Soal No 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"]
daftarHewan.sort()
console.log(daftarHewan)

// Soal no 2
function introduce(){
    return "Nama saya "+ data.name +", umur saya " + data.age + " tahun, alamat saya di "+ data.address +", dan saya punya hobby yaitu " + data.hobby +" !"
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan)

//Soal no 3
function hurufVokal(str) {
	var huruf_vokal = 'aeiouAEIOU';
	var count = 0;

	for (var x = 0; x < str.length; x++) {
		if (huruf_vokal.indexOf(str[x]) !== -1) {
			count++;
		}
	}
	return count;
}
var hitung_1 = hurufVokal("Muhammad")
var hitung_2 = hurufVokal("Iqbal")
console.log(hitung_1 , hitung_2)

//Soal 4
function hitung($angka){
    var a = ($angka * 2) - 2
    return a
}
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8
