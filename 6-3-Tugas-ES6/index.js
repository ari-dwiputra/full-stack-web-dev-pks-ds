//Soal 1
const luasPersegiPanjang = (a, b) => {
  return a * b;
};
console.log(luasPersegiPanjang(5, 2))

const kelilingPersegiPanjang = (a, b) => {
  return 2 * (a + b);
};
console.log(kelilingPersegiPanjang(5, 2))

//Soal 2
const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => {
      console.log(`${firstName} ${lastName}`)
    }
  }
}
//Driver Code 
newFunction("William", "Imoh").fullName()

//Soal 3
let newObject = [
  'Muhammad',
  'Iqbal Mubarok',
  'Jalan Ranamanyar',
  'playing football',
]

const [firstName, lastName, address, hobby] = newObject

console.log(firstName, lastName, address, hobby)

//Soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west, ...east]
console.log(combined)

//Soal 5
const planet = "earth" 
const view = "glass" 
var before = `Lorem ${view} dolor sit amet consectetur adipiscing elit ${planet}`
console.log(before)
